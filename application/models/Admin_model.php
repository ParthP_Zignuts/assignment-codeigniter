<?php

	Class Admin_model extends CI_Model{


		public function auto_load(){

				$this->load->database();
				
				 $query = $this->db->get('customer');

				 $total_cust = $query->num_rows();

				 $this->session->set_tempdata('total_cust',$total_cust,86400);
				
				 $query1 = $this->db->get('item');

				 $total_item = $query1->num_rows();

				 $this->session->set_tempdata('total_item',$total_item,86400);

				 $query2 = $this->db->get('orders');

				 $total_orders = $query2->num_rows();

				 $this->session->set_tempdata('total_orders',$total_orders,86400);

				 $query3 = $this->db->get_where('admin',array("active= "=>1));

				 $total_Admin = $query3->num_rows();

				 $this->session->set_tempdata('total_Admin',$total_Admin,86400);

		}
		//login user
		public function login($email,$password){


				$this->db->where('email',$email);
				$this->db->where('password',$password);
				$this->db->where('active',1);

				$result = $this->db->get('admin');
				$result1 = $result->result_array();
				//var_dump($result1);
				if($result->num_rows() == 1 ){
									//set_tempdata('name','parth',86400);
					$this->session->set_tempdata('id',$result1[0]['id'],86400);			
					$this->session->set_tempdata('name',$result1[0]['name'],86400);
					$this->session->set_tempdata('created_at',$result1[0]['created_at'],86400);

					return $result->row(0)->id;

				}else{

					return false;
				}
			}

		//register admin
		public function register($enc_password){
			//User data array 
			
			$data = array(

				'name' 		=> $this->input->post('name'),
				'email' => $this->input->post('email'),
				'password' => $enc_password

			);

			return $this->db->insert('admin',$data);
		}
		//check username is exist or not
		public function check_username_exists($username){
			$query = $this->db->get_where('users',array('username'=>$username));

			if(empty($query->row_array())){
					return true;
				}else{
						return false;
				}
		}
		//check email is exist or not
		public function check_email_exists($email){
			
			$query = $this->db->get_where('admin',array('email'=>$email));

			if(empty($query->row_array())){
					return true;
				}else{
						return false;
				}
				
		}
		//get all admin data 
		public function get_admin_data(){

				 $this->load->database();
				
				 $query = $this->db->get('admin');

				 return($query->result_array());

		}
		//get admin data by id
		
		public function get_adminby_id($id){

				 $this->load->database();
				 $this->db->where('id',$id);
				 $query = $this->db->get('admin');

				 return($query->result_array());

		}
		//edit admin data
		public function edit_admin($id){

					$this->load->database();

					$data = array(

							"name" => $this->input->post('name'),
							"email" => $this->input->post('email'),		
							"active" => $this->input->post('Active')

					);
					//var_dump($data);
					$this->db->where('id',$id);
					return $this->db->update("admin",$data);
		}

		//edit admin profile
		public function edit_profile($id){

					$this->load->database();

					$data = array(

							"name" => $this->input->post('name'),
									
							"active" => $this->input->post('Active')

					);
					//var_dump($data);
					$this->db->where('id',$id);
					return $this->db->update("admin",$data);
		}
		//update admin profile
		public function update_profile($id){

					$this->load->database();

					$data = array(
							"email" => $this->input->post('email'),
							"name" => $this->input->post('name'),
							//"password" => md5($this->input->post('password'))
							

					);
					//var_dump($data);
					$this->db->where('id',$id);
					return $this->db->update("admin",$data);
		}
		//update admin profile
		public function update_passowrd($id){

					$this->load->database();

					$data = array(
							"Password" => md5($this->input->post('new_pwd')),
						);
					//var_dump($data);
					$this->db->where('id',$id);
					return $this->db->update("admin",$data);
		}

		//delete admin
		public function admin_delete($id){

					$this->db->where('id', $id);
					return $this->db->delete('admin');

		}

		
		


	}

?>