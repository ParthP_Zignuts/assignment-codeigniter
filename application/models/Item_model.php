<?php

	Class Item_model extends CI_Model{

		//for count how many customers , orders , and items in current time
			public function __construct(){
			 	$this->Admin_model->auto_load();
			 }
		//get all items 
			public function get_items(){

				 $this->load->database();
				
				 $query = $this->db->get('item');

				 return($query->result_array());

			}

			//get all items 
			public function get_items_byid($item_id){

				 $this->load->database();
				
				 $query = $this->db->get_where('item',array("ID ="=> $item_id));

				 return($query->result_array());

			}

			//get all items 
			public function get_items_byname($item_name){

				 $this->load->database();
				
				 $query = $this->db->get_where('item',array("Name ="=> $item_name));

				 return($query->result_array());

			}

			//get Category 
			public function get_Category(){

				 $this->load->database();
				
				 $query = $this->db->get('Category');

				 return($query->result_array());

			}



			//add Items
			public function add_item($Image){

				
				$this->load->database();
				
				$data = array(

				 		"ItemNumber" => $this->input->post('ItemNumber'),
				 		"Name" => $this->input->post('Name'),
				 		"Category" => $this->input->post('Category'),
				 		"Description" => $this->input->post('Description'),
				 		"Image" => $Image,
				 		"Price" => $this->input->post('Price')

				 );
				
					return $this->db->insert("item",$data);


			}

			//update Items
			public function item_update($data){
				//print_r($data);
				$this->load->database();
				
				$data = array(

						"ItemNumber" =>$data['ItemNumber'],
				 		"Name" => $this->input->post('Name'),
				 		"Category" => $this->input->post('Category'),
				 		"Description" => $this->input->post('Description'),
				 		"Image" => $data['Image'],
				 		"Price" => $this->input->post('Price')

				 );

				 //print_r($data);
				 $this->db->where('ItemNumber', $data['ItemNumber']);
    			 return $this->db->update('item', $data);

			}
			

			//delete Items
			public function item_delete($item_id){
			
					$this->db->where('ID', $item_id);
					return $this->db->delete('item'); 

			}

			public function get_items_by_Ordid($order_id){
				
				$query_det=$this->db->get_where('order_details', array('Order_ID =' => $order_id));
				$order_details = $query_det->result_array();
				$num_items =  $query_det->num_rows();
				$c=0;
					while($c<$num_items){
						$query_item =$this->db->get_where('item', array('ID =' => $order_details[$c]['Item_Id']));
						$item_data[] = $query_item->result_array();
						$c++;
					}
					return($item_data);
			}
			

			
}
?>