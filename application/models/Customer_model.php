<?php

	Class Customer_model extends CI_Model{


			//for count how many customers , orders , and items in current time
			public function __construct(){
			 	$this->Admin_model->auto_load();
			 }
		//get Customer 
			public function get_Customer(){

				 $this->load->database();
				
				 $query = $this->db->get('customer');

				 $total_cust = $query->num_rows();

				 $this->session->set_tempdata('total_cust',$total_cust,86400);
				 
				 return($query->result_array());

			}

		//get customer Id 
			public function get_customer_byid($customer_id){

				 $this->load->database();
				
				 $query = $this->db->get_where('customer',array("CID ="=> $customer_id));

				 return($query->result_array());

			}

		//insert Customer 
			public function add_Customer(){

				$this->load->database();
				
				$data = array(

				 		"Name" => $this->input->post('Name'),
				 		"Email" => $this->input->post('Email'),
				 		"Phone" => $this->input->post('Phone'),
				 		"Address" => $this->input->post('Address')

				 );
				
				return $this->db->insert("customer",$data);

			}

			//Delete Customer 
			public function customer_delete($customer_id){

				    $this->db->where('CID', $customer_id);
					return $this->db->delete('customer'); 
			}

			//update Category 
			public function Customer_update($customer_id){

				$this->load->database();
				
				$data = array(

						"CID" =>$customer_id,
				 		"Name" => $this->input->post('Name'),
				 		"Email" => $this->input->post('Email'),
				 		"Phone" => $this->input->post('Phone'),
				 		"Address" => $this->input->post('Address')
				 	);

				//print_r($data);
				 $this->db->where('CID', $data['CID']);
    			 return $this->db->update('customer', $data);

			}
			
	}