<?php

	Class Category_model extends CI_Model{
		
		//for count how many customers , orders , and items in current time
		public function __construct(){
			 	$this->Admin_model->auto_load();
			 }

		public function add_Category(){
			$this->load->database();
				
				$data = array(

				 		"Name" => $this->input->post('Name'),
				 		"Description" => $this->input->post('Description')
				 	);
				
					return $this->db->insert("category",$data);
		}

		//get Category by id
			public function get_Category_id($id){

				 $this->load->database();
				
				 $query = $this->db->get_where('Category',array('ID='=> $id));

				 return($query->result_array());

			}

		//delete Category 
			public function category_delete($category_id){
				
				   $this->db->where('ID', $category_id);
				   return $this->db->delete('category'); 

			}

		//update Category 
			public function category_update($category_id){

				$this->load->database();
				
				$data = array(

						"ID" =>$category_id,
				 		"Name" => $this->input->post('Name'),
				 		"Description" => $this->input->post('Description')
				 	);

				//print_r($data);
				 $this->db->where('ID', $data['ID']);
    			 return $this->db->update('category', $data);

			}
	}