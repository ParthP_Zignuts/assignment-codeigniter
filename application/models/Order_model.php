<?php

	Class Order_model extends CI_Model{

			//for count how many customers , orders , and items in current time
			 public function __construct(){
			 	$this->Admin_model->auto_load();
			 }
		//get all orders 
			public function get_orders(){

				 $this->load->database();
				
				 /*$this->db->join('order_details','orders.Id = order_details.order_Id');*/

				 $this->db->join('customer','customer.CID = orders.Customer');

				 $query = $this->db->get('orders');

				 return($query->result_array());

			}

			//get all status 
			public function get_status(){
				 $this->load->database();
				
				 $query = $this->db->get('status');

				 return($query->result_array());
			}

			//add status 
			public function status_add(){

				$data = array(

				 		"Name" => $this->input->post('Name'),
				 		"Color" => $this->input->post('Color')
				 	 );

				return $this->db->insert("status",$data);
			}

			//get all items 
			public function get_orders_alldeta($order_id){

				 $this->load->database();
				
				 /*$this->db->join('order_details','orders.Id = order_details.order_Id');*/
				 
				 $orders = $this->db->get_where('orders',array("ID =" => $order_id));

				 $order =  $orders->result_array();

				 $order_details = $this->db->get_where('order_details',array("Order_Id =" => $order_id));

				 $data = array("orders" => $orders->result_array() , "order_details" => $order_details->result_array() );
				 

				 $customer = $this->db->get_where('customer',array("CID =" => $data['orders'][0]['Customer']));

				 //$query = $this->db->get('item');

				 $temp_order_datails = $this->db->get_where('temp_order_datails',array("OrderNumber =" => $order[0]['OrderNumber']));

				 $data = array("orders" => $orders->result_array() , "order_details" => $order_details->result_array() ,"customer_name" => $customer->result_array() ,"temp_order_datails" => $temp_order_datails->result_array() );
				
				 //echo $data['orders'][0]['Customer']."<br>";
				 
				 return($data);

			}

			//get all temp order data
			public function get_tmp_data(){

				 $this->load->database();
				 $query =$this->db->get_where('temp_order_datails', array('OrderNumber =' => $this->session->tempdata('OrderNumber')));

				 $this->session->set_tempdata("num_orders",$query->num_rows(),84600);

				 return($query->result_array());
			}

			//add item in to cart 
			public function add_cart(){
					
				$this->load->database();

				$query2 =$this->db->get_where('item', array('ID =' => $this->input->post('Item')));
				$item_data = $query2->result_array();
				
				$data = array(

				 		"OrderNumber" => $this->input->post('OrderNumber'),
				 		"Customer" => $this->input->post('Customer'),
				 		"Item_Id" => $this->input->post('Item'),
				 		"Qty" => $this->input->post('Qty'),
				 		"Price" => $this->input->post('Qty')*$item_data[0]['Price']
				 	 );
				$this->session->set_tempdata('OrderNumber',$this->input->post('OrderNumber'),600);

				$this->session->set_tempdata('CustomerId',$this->input->post('Customer'),600);

				return $this->db->insert("temp_order_datails",$data);
			}

			//edit item in to cart 
			public function edit_cart($OrderNumber){

				$this->load->database();

				if($this->input->post('Status') == 0){


					$query2 =$this->db->get_where('item', array('ID =' => $this->input->post('Item')));
				$item_data = $query2->result_array();
				
				$data = array(

				 		"OrderNumber" => $OrderNumber,
				 		"Customer" => $this->session->tempdata('CustomerId'),
				 		"Item_Id" => $this->input->post('Item'),
				 		"Qty" => $this->input->post('Qty'),
				 		"Price" => $this->input->post('Qty')*$item_data[0]['Price']
				 	 );

				$this->db->insert("temp_order_datails",$data);

				$data1 = array(

						"Order_Id" => $this->session->tempdata('OrderID'),
				 		"OrderNumber" => $OrderNumber,
				 		"Item_Id" => $this->input->post('Item'),
				 		"Qty" => $this->input->post('Qty'),
				 		"Price" => $this->input->post('Qty')*$item_data[0]['Price']
				 	 );

				$this->db->insert("order_details",$data1);

				}else{


				$data3 = array(
					'Status'=>$this->input->post('Status')
				);

				$this->db->where('ID', $this->session->tempdata('OrderID'));

    			if($this->db->update('orders',$data3))
    			return $this->session->set_flashdata('order_sts_updated','Order status is updated');

				}
	}
			//place order
			public function place_order($data){
					
				$this->load->database();

				$count = 0;
				$Price = 0; 

				

				while($count<$this->session->tempdata('num_orders')){

					
						$data2 = array(
								
								"OrderNumber" => $this->session->tempdata('OrderNumber'),
								"Item_Id" => $data['temp_data'][$count]['Item_Id'],
						 	    "Qty" =>  $data['temp_data'][$count]['Qty'],
						 		"Price" => $data['temp_data'][$count]['Price']
							 );

						$this->db->insert("order_details",$data2);

						$Price = $Price + $data['temp_data'][$count]['Price'];

						$count++;

						}

						$data = array(

				 		"OrderNumber" => $data['temp_data'][0]['OrderNumber'],
				 		"Customer" => $data['temp_data'][0]['Customer'],
				 		"Date" => date('Y-m-d H:i:s'),
				 		"Price" => $Price,
				 		"Status" => "1"
				 	 );


						$this->db->insert("orders",$data);
					
						$query =$this->db->get_where('orders =', array('OrderNumber' =>  $this->session->tempdata('OrderNumber')));

						$order_data = $query->result_array();

						//print_r($order_data);

						if(empty($order_data))
								$Order_Id = 1;
							else
								$Order_Id = $order_data[0]['ID'];
						
						$data3 = array(
							'Order_Id'=>$Order_Id
						);

						$this->db->where('OrderNumber', $this->session->tempdata('OrderNumber'));
    					$this->db->update('order_details',$data3);

						$this->session->unset_tempdata('OrderNumber');
						$this->session->unset_tempdata('CustomerId');
				}

			public function place_cart_order($data){
				// echo "<pre>";
				// print_r($data);

				foreach($data as $item_data)
    				{
    					$data = array(
	    					"OrderNumber" => $this->session->tempdata('OrderNumber'),
	    					"Customer" => $this->session->tempdata('Customer'),
	    					"Item_Id" => $item_data['Item'],
					 		"Qty" => $item_data['Qty'],
					 		"Price" => $item_data['Price']
					 	);

						$this->db->insert("temp_order_datails",$data);
    				
    					
    				}
    				
			}

			//add cart data into orders table 
			public function add_cartData_orders(){
					$data1 = array(
	    					"OrderNumber" => $this->session->tempdata('OrderNumber'),
	    					"Customer" => $this->session->tempdata('Customer'),
	    					"Date" =>  date('Y-m-d H:i:s'),
					 		"Price" => 0,
					 		"Status" => 1,
					 	);

						$this->db->insert("orders",$data1);

						$query =$this->db->get_where('orders', array('OrderNumber =' => $this->session->tempdata('OrderNumber')));
					    return($query->result_array());

			}

			//add cart data into order details table 
			public function add_cartData_order_details($data){
				
				foreach($data as $item_data)
    				{
    					$data = array(
	    					"OrderNumber" => $this->session->tempdata('OrderNumber'),
	    					"Item_Id" => $item_data['Item'],
					 		"Qty" => $item_data['Qty'],
					 		"Price" => $item_data['Price'],
					 		"Order_ID" => $item_data['OrderID'],
					 	);

						$this->db->insert("order_details",$data);
    					
    					$data2 = array(
	    					"Price" => $item_data['total']
	    				);

    					$this->db->where('ID', $item_data['OrderID']);
    			        return $this->db->update('orders', $data2);
    					
    				}

    				$this->session->unset_tempdata('OrderNumber');
    				$this->session->unset_tempdata('Customer');
    				return;

			}

			//delete order
			public function delete_order($order_id){

				$query =$this->db->get_where('orders', array('ID =' => $order_id));
				$order_data = $query->result_array();

				
						  $this->db->where('id', $order_id);
					      $this->db->delete('orders'); 

					      $this->db->where('Order_Id', $order_id);
					      $this->db->delete('order_details'); 

					      $this->db->where('OrderNumber', $order_data[0]['OrderNumber']);
					      return $this->db->delete('temp_order_datails'); 


				}

			//delete status
			public function Status_delete($Status_id){

				$this->db->where('ID', $Status_id);
			    return $this->db->delete('status'); 

			}
			//get Status by id
			public function get_Status_id($Status_id){

				 $this->load->database();
				
				 $query = $this->db->get_where('status',array('ID='=> $Status_id));

				 return($query->result_array());

			}

			//update Status
			public function Status_update($Status_id){

				$this->load->database();
				
				$data = array(

						"ID" =>$Status_id,
				 		"Name" => $this->input->post('Name'),
				 		"Color" => $this->input->post('Color')
				 	);

				//print_r($data);
				 $this->db->where('ID', $data['ID']);
    			 return $this->db->update('status', $data);

			}

			//update place order
			public function update_cart($data){
				
				
    			//echo "<pre>";
    			//print_r($data);
				    
    			 foreach($data as $key => $val)
    				{
    					$data = array(

				 		"Qty" => $val,
				 		"Price" => $data['Item_Price']*$val

				 	     );

    					$this->db->where('OrderNumber',$this->session->tempdata('OrderNumber'));
    					$this->db->where('Item_Id', $key);
    					$this->db->update('temp_order_datails',$data);

    					$this->db->where('OrderNumber',$this->session->tempdata('OrderNumber'));
    					$this->db->where('Item_Id', $key);
    					$this->db->update('order_details',$data);
    					
    					
    				}
			}
			//update place order
			public function update_place_order($data){

				$this->load->database();

				$count = 0;
				$Price = 0; 

				while($count<$this->session->tempdata('num_orders')){


						$Price = $Price + $data['temp_data'][$count]['Price'];

						$count++;

				}

					$data = array(

				 		"Date" => date('Y-m-d H:i:s'),
				 		"Price" => $Price,
				 		"Status" => "1"

				 	 );

					//var_dump($data);
					$query =$this->db->get_where('orders =', array('OrderNumber' =>  $this->session->tempdata('OrderNumber')));

					$order_data = $query->result_array();

						
					$this->db->where('OrderNumber', $this->session->tempdata('OrderNumber'));

    				$this->db->update('orders',$data);
				
					$this->session->unset_tempdata('OrderNumber');
					  
				}

				public function del_item_cart($item_id){

					$this->db->where('OrderNumber',$this->session->tempdata('OrderNumber'));
					$this->db->where('Item_Id',$item_id);
					$this->db->delete('temp_order_datails');

					$this->db->where('OrderNumber',$this->session->tempdata('OrderNumber'));
					$this->db->where('Item_Id',$item_id);
					return $this->db->delete('order_details');
				}
				public function edit_item_cart($item_id){

						$order_id = $this->session->tempdata('OrderID');
						$OrderNumber = $this->session->tempdata('OrderNumber');
						echo "Qty".$this->input->post('Qty');
						$data = array(

				 		"Qty" => $this->input->post('Qty'),
				 		
				 		 );

						$this->db->where('OrderNumber', $this->session->tempdata('OrderNumber'));
						$this->db->where('Item_Id', $item_id);
	    				//$this->db->update('temp_order_datails',$data);
					
						//$this->session->unset_tempdata('OrderNumber');
						//return $order_id; 

				}

				public function get_orderData_by_ordId($order_id){

					$query =$this->db->get_where('orders', array('ID =' => $order_id));
					$order_data = $query->result_array();

					$query_det=$this->db->get_where('order_details', array('Order_ID =' => $order_id));
					$order_details = $query_det->result_array();
					
					$query_cust =$this->db->get_where('customer', array('CID =' => $order_data[0]['Customer']));
					$customer_data = $query_cust->result_array();

					$query_admin =$this->db->get_where('admin', array('id =' => $this->session->tempdata('id')));
					$admin_data = $query_admin->result_array();

					$num_items =  $query_det->num_rows();
					$c=0;
					while($c<$num_items){
						$query_item =$this->db->get_where('item', array('ID =' => $order_details[$c]['Item_Id']));
						$item_data[] = $query_item->result_array();
						$c++;
					}

					$order_info = array("order_data"=>$order_data,"order_details"=>$order_details,"customer_data"=>$customer_data,"admin_data"=>$admin_data,"item_data"=>	$item_data);

					return $order_info;
				}

				public function get_customer_by_ordId($order_id){

					$query =$this->db->get_where('orders', array('ID =' => $order_id));
					$order_data = $query->result_array();
					return $order_data;

				}

		}
?>
