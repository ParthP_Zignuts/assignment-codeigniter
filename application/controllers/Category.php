<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {


	//get category 
	public function category_data(){
			$data['Category'] = $this->Item_model->get_Category();
			$data['page'] = "category/category_data";
			$this->load->view('templates/admin_template',$data);
	}

	//add category form
	public function add(){

			//$data['items'] = $this->Category_model->add_Category();
			$data['page'] = "category/add_category";
			$this->load->view('templates/admin_template',$data);

	}

	//add category
	public function add_category(){

			$data['category'] = $this->Category_model->add_Category();

			$this->session->set_flashdata('category_inserted','categor is inserted');	

			redirect(base_url().'Category/category_data');

	}

	//delete category
	public function Category_delete($category_id){

			$delete = $this->Category_model->category_delete($category_id);

			if($delete == TRUE){
				$this->session->set_flashdata('Category_deleted','Category is deleted');
				redirect(base_url().'Category/category_data');
			}

	}

	//edit category
	public function Category_edit($category_id){

			$data['Category'] = $this->Category_model->get_Category_id($category_id);
			$data['page'] = "category/edit_category";
			$this->load->view('templates/admin_template',$data);
			
	}

	//update category
	public function Category_update($category_id){

		    $this->Category_model->category_update($category_id);
			
			$this->session->set_flashdata('category_updated','Category is Updated');	

			redirect(base_url().'Category/category_data');


	}

	

}