<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct()
 	{
		   parent::__construct();
		   $this->load->model('htmltopdf_model');
		  // $this->load->library('pdf');
	}

	public function index()
	{
		echo $_POST['val'];  //$data will contain the_id
			//do some processing
			//echo "yourVoteCount";
	}
	
	//display all orders 
	public function all_order()
	{
		
			$data['orders'] = $this->Order_model->get_orders();
			$c=0;
			while($c<count( $this->Order_model->get_orders()))
			{
				$data['status'][$c] = $this->Order_model->get_Status_id($data['orders'][$c]['Status']);
				$c++;
				/*echo "<pre>";
				print_r($data['status']);*/
			}
			
			$data['page'] = "order/order_data";
			$this->load->view('templates/admin_template',$data);

	}

	//display all status 
	public function all_status()
	{
		
			$data['Status'] = $this->Order_model->get_status();
			$data['page'] = "status/status_data";
			$this->load->view('templates/admin_template',$data);

	}

	  //delete Status
		public function Status_delete($Status_id){

				$delete = $this->Order_model->Status_delete($Status_id);

				if($delete == TRUE){
					$this->session->set_flashdata('Status_deleted','Status is deleted');
					redirect(base_url().'Order/all_status');
				}

		}
	//add status page
	public function add_status(){

			$data['page'] = "status/add_status";
			$this->load->view('templates/admin_template',$data);

	}
	//add status
	public function status_add(){

			$this->Order_model->status_add();
			$this->session->set_flashdata('status_add','Status is added');
			redirect(base_url().'Order/all_status');

	}

	//edit status
	public function Status_edit($Status_id){

			$data['status'] = $this->Order_model->get_Status_id($Status_id);
			$data['page'] = "status/edit_status";
			$this->load->view('templates/admin_template',$data);
			
	}

	//update status
	public function Status_update($Status_id){

		    $this->Order_model->Status_update($Status_id);
			
			$this->session->set_flashdata('status_updated','Status is Updated');	

			redirect(base_url().'Order/all_status');
	}

	//add Order page
	public function add(){

			$data['Customer'] = $this->Customer_model->get_Customer();
			$data['Items'] = $this->Item_model->get_items();
			$data['temp_data'] = $this->Order_model->get_tmp_data();
			$data['page'] = "order/add_order";
			$this->load->view('templates/admin_template',$data);

	}

	//add cart
	public function add_cart(){

			
			//echo $order_id;
			$OrderID = $this->input->post('OrderID');
   			$CustomerId = $this->input->post('CustomerId');

   			//echo json_encode($CustomerId);
   			$this->session->set_tempdata('OrderNumber',$OrderID,600);
			$this->session->set_tempdata('Customer',$CustomerId,600);

			/*$this->Order_model->add_cart();
	
			redirect(base_url().'Order/add');*/
			// echo $this->session->tempdata('OrderNumber');
			// echo $this->session->tempdata('Customer');
			redirect(base_url().'Order/add');

	}

	//add order
	public function place_order(){

			$Item = $this->input->post('Item');
			$Qty = $this->input->post('Qty');
			$Price = $this->input->post('Price');
			$sub_total = $this->input->post('sub_total');
			
			$c=0;
			while($c<count($Item)){
				$data['item_data'] = array("Item"=>$Item[$c],"Qty"=>$Qty[$c],"Price"=>$Price[$c]*$Qty[$c]);
				$this->Order_model->place_cart_order($data);
				
				//var_dump($Item[$c]);
				// $update_data = array($Item_data[$c][0]['ID']=>$Qty[$c],"Item_Price"=>$Item_data[$c][0]['Price']); 
				// $this->Order_model->update_cart($update_data);
				$c++;
			}
			$data1['Order_data'] = $this->Order_model->add_cartData_orders();
			//$data['OrderID'] = $data['Order_data'][0]['ID'];
			$c=0;
			$total=0;
			while($c<count($Item)){

				$data['item_data'] = array("Price"=>$sub_total[$c]);
				$total = $total + $data['item_data']['Price'];
				$data['item_data'] = array("Item"=>$Item[$c],"Qty"=>$Qty[$c],"Price"=>$Price[$c]*$Qty[$c],"OrderID"=>$data1['Order_data'][0]['ID'],"total"=>$total);
				$this->Order_model->add_cartData_order_details($data);
				$c++;
			}
			// $data['temp_data'] = $this->Order_model->get_tmp_data();
			// $this->Order_model->place_order($data);
			$this->session->unset_tempdata('OrderNumber');
			$this->session->unset_tempdata('Customer');
			$this->session->set_flashdata('order_placed','Order is placed');
			redirect(base_url().'Order/all_order');

	}

	//delete order
	public function order_delete($order_id){
			
			$delete = $this->Order_model->delete_order($order_id);

			if($delete == TRUE){
				$this->session->set_flashdata('order_deleted','Order is deleted');
				redirect(base_url().'Order/all_order');
			}
	}

	//edit order page
	public function order_edit_page($order_id){
			
			$data['order_data'] = $this->Order_model->get_orders_alldeta($order_id);
			$data['CurrentStatus'] = $this->Order_model->get_Status_id($data['order_data']['orders'][0]['Status']);
			$data['item_data'] = $this->Item_model->get_items_by_Ordid($order_id);
			$data['Items'] = $this->Item_model->get_items();
			$data['Status'] = $this->Order_model->get_status();
			$data['page'] = "order/order_edit";
			$this->load->view('templates/admin_template',$data);
	}

	//edit order
	/*public function order_edit($order_id){
			
			$edit = $this->Order_model->edit_order($order_id);

			if($edit == TRUE){
				$this->session->set_flashdata('order_edited','Order is edited');
				redirect(base_url().'Order/all_order');
			}
	}*/

	//update order
	public function update_order($OrderNumber){
		/*print_r($OrderNumber);
		echo $this->session->tempdata('OrderID');*/
		$this->Order_model->edit_cart($OrderNumber);
		redirect(base_url().'Order/order_edit_page/'.$this->session->tempdata('OrderID'));

	}

	//add order
	public function update_place_order(){

			//edit item data in cart 
			$Item_name= $this->input->post('Item_name');
			$Qty = $this->input->post('Qty');
			$c=0;
			while($c<count($Item_name)){
				$Item_data[$c] = $this->Item_model->get_items_byname($Item_name[$c]);
				$update_data = array($Item_data[$c][0]['ID']=>$Qty[$c],"Item_Price"=>$Item_data[$c][0]['Price']); 
				$this->Order_model->update_cart($update_data);
				$c++;
			}
			
				

			$data['temp_data'] = $this->Order_model->get_tmp_data();
			$this->Order_model->update_place_order($data);	
			$this->session->set_flashdata('order_updated','Order is updated');
			$this->session->unset_tempdata('OrderNumber');
			$this->session->unset_tempdata('Customer');
			redirect(base_url().'Order/all_order');

		}

	//delete item in cart
	public function del_item_cart($item_id){

			$delete = $this->Order_model->del_item_cart($item_id
			);

			redirect(base_url().'Order/order_edit_page/'.$this->session->tempdata('OrderID'));
	}

	//email send
	public function send_mail()
         {
         		
                   $config = Array(
                     'protocol' => 'smtp',
                     'smtp_host' => 'ssl://smtp.googlemail.com',
                     'smtp_port' => 465,
                     'smtp_user' => 'parthprajapati1056@gmail.com', // change it to yours
                     'smtp_pass' => 'techparth@210@@', // change it to yours
                     'mailtype' => 'html',
                     'charset' => 'iso-8859-1',
                     'wordwrap' => TRUE
                        );

                $message = 'hii';
	            $this->load->library('email', $config);
	            $this->email->set_newline("\r\n");
	            $this->email->from('parthprajapati1056@gmail.com'); // change it to yours
	            $this->email->to($this->input->post('email'));// change it to yours
	            $this->email->subject('Resume from JobsBuddy for your Job posting');
	            $this->email->message($message);

       
         if($this->email->send()) 
         {
               $this->session->set_flashdata("email_sent","Email sent successfully."); 
         }

         else {
               show_error($this->email->print_debugger());  
               //$this->session->set_flashdata("email_sent","Error in sending Email.");

               $this->load->view('email_form'); 
        }
      }  

      public function View($order_id){
      		$data['OrderID'] = $order_id;
      		$data['Oredr_all_data'] = $this->Order_model->get_orderData_by_ordId($order_id);
      		$data['page'] = "Invoice/orderInvoice";
			$this->load->view('templates/admin_template',$data);	

      }

	   public function pdfdetails($order_id)
		 {
		  if($this->uri->segment(3))
		  {
		   $customer_id = $this->uri->segment(3);
		   $html_content = '';
		  
		   $html_content = '<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">';

		  
		   $html_content .= $this->htmltopdf_model->fetch_single_details($order_id);
		   //var_dump($html_content);
		   /*$page = file_get_contents(base_url()."/orderInvoice.html");
		   var_dump($page);*/
		   /*$this->pdf->loadHtml($html_content);
		   
		   $this->pdf->render();

		   $this->pdf->stream("ORD".$order_id.".pdf", array("Attachment"=>1));*/
		   	  $data['Oredr_all_data'] = $this->Order_model->get_orderData_by_ordId($order_id);
		   	  //echo "<pre>";print_r($data);
		      $this->pdf->load_view('Invoice/pdfView',$data);
			  $this->pdf->render();
			  $this->pdf->stream("ORD".$order_id.".pdf",array("Attachment"=>1));
		   
		  }
		 }

		public function  sendEmail($order_id){
			$data['Order_data'] = $this->Order_model->get_customer_by_ordId($order_id);
			$data['Customer_data'] = $this->Customer_model->get_customer_byid($data['Order_data'][0]['Customer']);
			 

			 //email configuration
			 $config = Array(
                     // 'protocol' => 'smtp',
                     // 'smtp_host' => 'ssl://smtp.googlemail.com',
                     // 'smtp_port' => 465,
                     // 'smtp_user' => 'parthprajapati1056@gmail.com', // change it to yours
                     // 'smtp_pass' => 'techparth@210@@', // change it to yours
                     // 'mailtype' => 'html',
                     // 'charset' => 'iso-8859-1',
                     // 'wordwrap' => TRUE
                     
                     //threw MailTrap 
                      'protocol' => 'smtp',
					  'smtp_host' => 'smtp.mailtrap.io',
					  'smtp_port' => 2525,
					  'smtp_user' => '2db62081eab299',
					  'smtp_pass' => '39c9729305e224',
					  'crlf' => "\r\n",
					  'newline' => "\r\n"
                        );

            $message = 'Order Invoice : ';
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('parthprajapati1056@gmail.com'); 
            $this->email->to($data['Customer_data'][0]['Email']);
            $this->email->subject('Order Invoice : from Zignuts Technolab,Gandhinagar');
            $this->email->attach('/home/ztlab02/Downloads/ORD'.$order_id.'.pdf');
            $this->email->message($message);
            

       
         if($this->email->send()) 
         {
               $this->session->set_flashdata("email_sent","Email sent successfully."); 
               redirect(base_url().'Order/all_order');
         }

         else {
               show_error($this->email->print_debugger());  
               $this->session->set_flashdata("email_not_sent","Error in sending Email.");
               redirect(base_url().'Order/all_order');
            }
			
		}

		//set data with ajax 
		public function add_order_ajx($item_id){
			
			
			    if($item_id != null){
			        
			        $this->load->database();
				
				   $query = $this->db->get_where('item',array("ID= "=>$item_id));
				   $data = $query->result_array();
			       echo json_encode($data);

			        //echo "Affected rows: " . $this->$query->affected_rows();
			    }
		}

		//Cancel Order
		public function Cancel_Order(){
			$this->session->unset_tempdata('OrderNumber');
			$this->session->unset_tempdata('Customer');
			redirect(base_url().'Order/all_order');
		}
}

