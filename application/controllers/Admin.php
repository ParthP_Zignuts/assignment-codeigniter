<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	
	public function index()
	{

		$this->Admin_model->auto_load();
		
		$this->load->view('admin/login');

	}

	public function login_valid(){

			
		    $this->form_validation->set_rules('email','Email','required');
			    
			$this->form_validation->set_rules('password','Password','required');
		
			if($this->form_validation->run() === FALSE ){
	
						$this->load->view('login/login');

					}else{

						//Get user name
						$email = $this->input->post('email');
						
						//Get encript password
						$password = md5($this->input->post('password'));

						$id = $this->Admin_model->login($email,$password);

					    if($id){

					        $admin_data = array(
								'id' => $id,
								'name' => $this->session->tempdata('name'),
								'loggedin' => 'true'
							);
					       // var_dump($admin_data);
					        
						/*if($this->session->mark_as_temp($admin_data,86400))
							echo "yes";
						else
							echo "no";*/
						$this->session->set_flashdata('admin_logedin','You are now Logged in');

						redirect('dashboard');

						}else{

							$this->session->set_flashdata('login_failed','Login is Invalid..');	
							$this->load->view('admin/login');

						}

				}
		}

		//logout admin
		public function logout(){
			
				//unset user data
				
				$this->session->unset_userdata('loggedin');
				$this->session->unset_userdata('user_id');
				$this->session->unset_userdata('username');

				$this->session->set_flashdata('logged_out','You are Logout');	

				redirect('Admin');

		}


		//sign up admin
			public function sign_up(){
				
				$this->load->view('admin/signup');

			}

		//register admin
			public function register(){

					$this->form_validation->set_rules('name','Name','required');
				    $this->form_validation->set_rules('email','Email','required|callback_check_email_exists');
				    $this->form_validation->set_rules('password','Password','required');
					$this->form_validation->set_rules('password2','ConfirmPassword','matches[password]');

				if($this->form_validation->run() === FALSE ){
		
				
							$this->load->view('admin/signup');

						
				}else{

							//Encrypt Password===
							$enc_password = md5($this->input->post('password'));

							//set message
							$this->session->set_flashdata('user_registered','You are now Register and can Log in');	

							$this->Admin_model->register($enc_password);

							
							redirect('Admin');

				}

			}

			function check_email_exists($email){
					$this->session->set_flashdata('check_email_exists','That email Taken , Please choose a different one.');

					if($this->Admin_model->check_email_exists($email)){
							return true;
					}else{
							return false;
					}
		}
		//Get Admin data
		public function admin_info(){

					$data['admin'] = $this->Admin_model->get_admin_data();
					$data['page'] = "admin/admin_data";
					//$res = array($result,$data);
					//var_dump($data);
					$this->load->view('templates/admin_template',$data);

		}

		//edit admin data
		public function admin_edit($id){
			
					$data['admin'] = $this->Admin_model->get_adminby_id($id);
					$data['page'] = "admin/admin_edit";
					$this->load->view('templates/admin_template',$data);
		}
		//update admin data
		public function admin_update($id){
			
					$data['admin'] = $this->Admin_model->edit_admin($id);
					$this->session->set_flashdata('admin_updated','Admin data are Updated');	
					redirect('Admin/admin_info');
				}

		//edit admin profile
		public function edit_profile($id){
			
					$data['admin'] = $this->Admin_model->get_adminby_id($id);
					$data['page'] = "admin/admin_profile_edit";
					$this->load->view('templates/admin_template',$data);
					
				}
		//update admin profile
		public function update_profile($id){
			
					$this->Admin_model->update_profile($id);
					$this->session->set_flashdata('admin_profile_updated','Admin data are Updated');	
					redirect(base_url().'dashboard');
				}
				
		//change admin passowerd
		public function edit_pwd($id){
		
				$data['admin'] = $this->Admin_model->get_adminby_id($id);
				$data['page'] = "admin/admin_change_pwd";
				$this->load->view('templates/admin_template',$data);
			}

		//change admin passowerd
		public function update_pwd($id)
		{
				$data['admin'] = $this->Admin_model->get_adminby_id($id);

				$password = md5($this->input->post('old_pwd'));

				$this->form_validation->set_rules('new_pwd','Password','required');
				$this->form_validation->set_rules('cnf_pwd','ConfirmPassword','matches[new_pwd]');

				if($data['admin'][0]['Password'] == $password){
					
				 if($this->form_validation->run() === FALSE ){

					$this->session->set_flashdata('cnf_pwd_not_match','Confirm Passowerd is not match');

					redirect('Admin/edit_pwd/'.$id);
				 }else{

				 	$this->Admin_model->update_passowrd($id);
					$this->session->set_flashdata('admin_pwd_updated','Admin Password is Updated');	
					redirect(base_url().'dashboard');

				 }

				}else{
					$this->session->set_flashdata('old_pwd_not_match','Old Passowerd is not match');	
					redirect('Admin/edit_pwd/'.$id);
				}

			}

		//change admin passowerd
			public function admin_delete($id)
			{
				$delete = $this->Admin_model->admin_delete($id);

				if($delete == TRUE){
					$this->session->set_flashdata('Admin_deleted','Admin is deleted');
					redirect(base_url().'Admin/admin_info');
				}
			}
}
		