<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	//get customer 
	public function customer_data(){
			$data['Customer'] = $this->Customer_model->get_Customer();
			$data['page'] = "customer/customer_data";
			$this->load->view('templates/admin_template',$data);
	}

	//add Customer
	public function add(){

			$data['page'] = "customer/add_customer";
			$this->load->view('templates/admin_template',$data);

	}

	//insert Customer
	public function add_Customer(){

			$this->Customer_model->add_Customer();

			$this->session->set_flashdata('customer_inserted','customer is inserted');	

			redirect(base_url().'Customer\customer_data');

	}

	//delete Customer
	public function customer_delete($customer_id){

			$delete = $this->Customer_model->customer_delete($customer_id);

			if($delete == TRUE){
				$this->session->set_flashdata('Customer_deleted','Customer is deleted');
				redirect(base_url().'Customer\customer_data');
			}

	}

	//edit Customer
	public function customer_edit($customer_id){

			$data['Customer'] = $this->Customer_model->get_customer_byid($customer_id);
			
			$data['page'] = "customer/edit_customer";
			
			$this->load->view('templates/admin_template',$data);
	}

	//update Customer
	public function update_customer($customer_id){

		    $this->Customer_model->Customer_update($customer_id);
			
			$this->session->set_flashdata('customer_updated','Customer is Updated');	

			redirect(base_url().'Customer\customer_data');
	}

}