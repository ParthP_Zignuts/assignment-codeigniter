<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	//display all items 
	public function all_item()
	{
		
			$data['items'] = $this->Item_model->get_items();
			$data['page'] = "item/item_data";
			$this->load->view('templates/admin_template',$data);

	}

	//add item
	public function add(){

			$data['Category'] = $this->Item_model->get_Category();
			$data['items'] = $this->Item_model->get_items();
			$data['page'] = "item/add_item";
			$this->load->view('templates/admin_template',$data);

	}

	//insert Item data
	public function add_item(){

					 $config['upload_path']   = './uploads/'; 
                     $config['allowed_types'] = 'gif|jpg|png'; 
                     $config['max_size']      = 100000; 
                     $config['max_width']     = 102400; 
                     $config['max_height']    = 768000;  
                     $this->load->library('upload', $config);
                        			
                     if ( ! $this->upload->do_upload('Image')) {

                                    $error = array('error' => $this->upload->display_errors()); 
                                 
                     }
            			
                     else { 
                                    $data = array('upload_data' => $this->upload->data()); 
                                    $Image = $_FILES['Image']['name'];
									
                           } 

			$this->Item_model->add_item($Image);

			$this->session->set_flashdata('item_inserted','Item is inserted');	

			redirect(base_url().'Item/All_item');

	}

	//item delete
	public function item_delete($item_id){

			$delete = $this->Item_model->item_delete($item_id);

			if($delete == TRUE){
				$this->session->set_flashdata('Item_deleted','Item is deleted');
				redirect(base_url().'Item/all_item');
			}
		}

	//item edit
	public function item_edit($item_id){

			$data['items'] = $this->Item_model->get_items_byid($item_id);

			$data['Category'] = $this->Item_model->get_Category();
	
			$data['Category_id'] = $this->Category_model->get_Category_id($data['items'][0]['Category']);
			
			$data['page'] = "item/edit_item";
			
			$this->load->view('templates/admin_template',$data);

			
		}

	public function update_item($ItemNumber){
			
					 $config['upload_path']   = './uploads/'; 
                     $config['allowed_types'] = 'gif|jpg|png|jpeg'; 
                     $config['max_size']      = 100000; 
                     $config['max_width']     = 102400; 
                     $config['max_height']    = 768000;  
                     $this->load->library('upload', $config);
                        			
                     if ( ! $this->upload->do_upload('Image')) {

                                    $error = array('error' => $this->upload->display_errors()); 
                                 	$Image = $this->session->tempdata('OldImage');
                     }
            			
                     else { 
                                    $data = array('upload_data' => $this->upload->data()); 
                                    $Image = $_FILES['Image']['name'];
								
                          } 

			$this->Item_model->item_update(array("ItemNumber"=>$ItemNumber,"Image"=>$Image));
			
			$this->session->set_flashdata('item_updated','Item is Updated');	

			redirect(base_url().'Item/All_item');

		}
}