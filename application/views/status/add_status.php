

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Status
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Status</a></li>
        <li class="active">Add Status</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Status</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open_multipart('Order/status_add'); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="Name" placeholder="Enter Name" class="form-control" required>
                </div>
 
                <div class="form-group">
                
                    <div class="form-group">
                      <label>Color picker with addon:</label>

                        <div class="input-group my-colorpicker2">
                          <input type="text" name="Color" placeholder="Slect Color" class="form-control" required>

                            <div class="input-group-addon">
                                <i></i>
                            </div>
                        </div>
                    <!-- /.input group -->
                  </div>
                    <!-- /.input group -->
              </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>






         