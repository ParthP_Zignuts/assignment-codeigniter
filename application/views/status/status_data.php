
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Current Active Order Status
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Current Active Order Status</li>
      </ol>
  

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box" style="overflow-x:auto;">
            <div class="box-header">
              <h3 class="box-title">Current Active Order Status</h3>
              <a href="<?php echo base_url(); ?>Order\add_status" class="btn btn-block btn-success" style="float: right;width: auto;">Add Status</a>

            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Color</th>
                  <th></th>
               </tr>
                </thead>
                <tbody>

            <?php foreach($Status as $status):?>
                <tr>
                  <td><?php echo $status['ID'];?></td>
                  
                  <td><?php echo $status['Name'];?></td>
                  
                  <td style="background-color: <?php echo $status['Color'];?>; width: 10px;"></td>
                  <td>
                    <a class="btn btn-primary"  href="<?php echo site_url('/Order/Status_edit/'.$status['ID']); ?>"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger"  href="<?php echo site_url('/Order/Status_delete/'.$status['ID']); ?>"><i class="glyphicon glyphicon-trash">
                  </td>
                </tr>
              <?php endforeach;?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Color</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


