<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Asignment Codeigniter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
   <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><i class="fa fa-tasks" aria-hidden="true"></i></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Assignment</b></span>
    </a>

  
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <?php if($this->session->flashdata('admin_logedin')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong> <?php echo ucfirst($this->session->flashdata('name')); ?> is Successully login.
       </div>

      <?php endif; ?>

       <?php if($this->session->flashdata('admin_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Admin data is Successully updated.
       </div>

      <?php endif; ?>

     <?php if($this->session->flashdata('admin_profile_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Admin Profile data is Successully updated.
       </div>

      <?php endif; ?>

     <?php if($this->session->flashdata('old_pwd_not_match')):?>

        <div class="alert alert-danger alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Error! </strong>Old Passowerd is not matched.
       </div>

    <?php endif; ?>
      

    <?php if($this->session->flashdata('cnf_pwd_not_match')):?>

        <div class="alert alert-danger alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Error! </strong>Confirm Passowerd is not match.
       </div>

    <?php endif; ?>

   <?php if($this->session->flashdata('admin_pwd_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Passowrd is Changed.
       </div>

    <?php endif; ?>

   <?php if($this->session->flashdata('item_inserted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Item is inserted.
       </div>

    <?php endif; ?>

  <?php if($this->session->flashdata('category_inserted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Category is inserted.
       </div>

    <?php endif; ?>

   <?php if($this->session->flashdata('customer_inserted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Customer is inserted.
       </div>

    <?php endif; ?>

    <?php if($this->session->flashdata('order_placed')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Order is Placed.
       </div>

    <?php endif; ?>

    <?php if($this->session->flashdata('order_deleted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Order is Deleted.
       </div>

    <?php endif; ?>
  
    <?php if($this->session->flashdata('order_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Order is Updated.
       </div>

    <?php endif; ?>
    
    <?php if($this->session->flashdata('Item_deleted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Item is Deleted.
       </div>

    <?php endif; ?>
    
    <?php if($this->session->flashdata('item_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Item is Updated.
       </div>

    <?php endif; ?> 

    <?php if($this->session->flashdata('Category_deleted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Category is Deleted.
       </div>

    <?php endif; ?> 

    <?php if($this->session->flashdata('category_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Category is Updated.
       </div>

    <?php endif; ?> 

    <?php if($this->session->flashdata('Customer_deleted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Customer is Deleted.
       </div>

    <?php endif; ?> 

     <?php if($this->session->flashdata('customer_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Customer is Updated.
       </div>

    <?php endif; ?> 

     <?php if($this->session->flashdata('Admin_deleted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Admin is Deleted.
       </div>

    <?php endif; ?> 

     <?php if($this->session->flashdata('email_sent')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success! </strong>Order Invoice Send Successfully.
       </div>

    <?php endif; ?> 

    <?php if($this->session->flashdata('email_not_sent')):?>

        <div class="alert alert-danger alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Error! </strong>Order Invoice Send Failed.
       </div>

    <?php endif; ?> 

    <?php if($this->session->flashdata('order_sts_updated')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Sucess! </strong>Order Status is Updated.
       </div>

    <?php endif; ?> 

     <?php if($this->session->flashdata('Status_deleted')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Sucess! </strong>Status is Deleted.
       </div>

    <?php endif; ?>

     <?php if($this->session->flashdata('status_add')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Sucess! </strong>Status is Added.
       </div>

    <?php endif; ?>

    <?php if($this->session->flashdata('status_updated')):?>

      <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Sucess! </strong>Status is Updated.
       </div>

    <?php endif; ?>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo ucfirst($this->session->userdata('name')); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->tempdata('name'); ?>

                  <small>Registred at :<?php echo date("d M, Y", strtotime($this->session->tempdata('created_at'))); ?></small>
                </p>
              </li>
             
              <!-- Menu Footer-->
            <li class="user-body"  style="font-size: 1px;">
             
              <div class="col-xs-12">
                <div class="row">
                <div class="text-center">
                  <a href=<?php echo base_url()."Admin/edit_profile/".$this->session->tempdata('id'); ?> class="col-xs-3 btn btn-default btn-flat">Profile</a>
                </div>
                <div style="padding-left:0px"class="text-center">
                  <a href=<?php echo base_url()."Admin/edit_pwd/".$this->session->tempdata('id'); ?> class="col-xs-5 btn btn-default btn-flat">ChangePassword</a>
                </div>
                <div style="padding-left:0px;"class="text-center">
                  <a href="<?php echo base_url();?>Admin\logout" class="col-xs-4 btn btn-default btn-flat">Sign out</a>
                </div>
              </div>
              </div>
            </li>

            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucfirst($this->session->userdata('name')); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="<?php echo base_url();?>dashboard">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
          
        </li>
        <li class="">
          <a href="<?php echo base_url(); ?>Admin/admin_info">
            <i class="fa fa-users"></i><span>Admin's Data</span>
          </a>
        </li>
        <li class="">
          <a href="<?php echo base_url();?>Item\All_item">
            <i class="fa fa-barcode"></i><span>Items</span>
          </a>
        </li>
        <li class="">
          <a href="<?php echo base_url();?>Category\category_data">
            <i class="fa fa-barcode"></i><span>Category</span>
          </a>
        </li>
        <li class="">
          <a href="<?php echo base_url();?>Customer\customer_data">
            <i class="ion ion-person-add"></i><span>Customer</span>
          </a>  
        </li>
         <li class="">
          <a href="<?php echo base_url();?>Order\all_order">
            <i class="ion ion-bag"></i><span>Orders</span>
          </a>  
        </li>
        <li class="">
          <a href="<?php echo base_url();?>Order\all_status">
            <i class="fa fa-clock-o" aria-hidden="true"></i><span>Status</span>
          </a>  
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
 
  
  		<?php  $this->load->view($page); ?>

 
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019 <a href="https://www.zignuts.com/">Zignuts Technolab , Gandhinagar</a>.</strong> All rights
    reserved.
  </footer>

 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- bootstrap color picker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

</script>

<script type="text/javascript">
 
 //dialog open when read more discription in category_data page
  $(document).on('click','.readmore',function(){
         var d = $(this).attr("data-Desc");
          $('#modelWindow').modal('show');
          $(".Description_view").html(d);
  });

</script>

</body>
</html>

