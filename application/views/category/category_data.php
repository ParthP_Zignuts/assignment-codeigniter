<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category Data Tables
        <small>advanced tables</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category Data tables</li>
      </ol>
  

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Category Data Table With Full Features</h3>
              <a href="<?php echo base_url(); ?>Category\add" class="btn btn-block btn-success" style="float: right;width: auto;">Add Category</a>

            </div>

            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th></th>
               </tr>
                </thead>
                <tbody>

            <?php foreach($Category as $data):?>
                <tr>
                  <td><?php echo $data['ID'];?></td>
                  
                  <td style="width:200px;"><?php echo word_limiter($data['Name'],2,'<span style="color: #243af2;" class="readmore" data-Desc="'.$data['Name'].'"> read more</span>');?></td>
                  
                  <td style="width:300px;"><?php echo word_limiter($data['Description'],4,'<span style="color: #243af2;" class="readmore" data-Desc="'.$data['Description'].'"> read more</span>');?></td>
                  
                  <td>
                    <a class="btn btn-primary"  href="<?php echo site_url('/Category/Category_edit/'.$data['ID']); ?>"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger"  href="<?php echo site_url('/Category/Category_delete/'.$data['ID']); ?>"><i class="glyphicon glyphicon-trash">
                  </td>
                </tr>
              <?php endforeach;?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- dialog box for read more discription -->
      <div class="modal fade" id="modelWindow" role="dialog">
            <div class="modal-dialog modal-sm vertical-align-center">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Description</h4>
                </div>
                <div class="modal-body">
                    <span class="Description_view"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                </div>
              </div>
            </div>
        </div>



