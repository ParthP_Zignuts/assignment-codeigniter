
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Category
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>
        <li class="active">Add Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Category</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open_multipart('Category/add_category'); ?>
              <div class="box-body">

              

                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="Name" placeholder="Enter Name" class="form-control" required>
                </div>

             
                 <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="3" name="Description" placeholder="Description" required></textarea>
                </div>
                 
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>


         