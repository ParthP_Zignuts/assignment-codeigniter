
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customers Data Tables
        <small>advanced tables</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customers</li>
      </ol>
  

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers Data Table With Full Features</h3>
              <a href="<?php echo base_url(); ?>Customer\add" class="btn btn-block btn-success" style="float: right;width: auto;">Add Customer</a>

            </div>

            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>

            <?php foreach($Customer as $data):?>
                <tr>
                  <td><?php echo $data['CID'];?></td>
                  <td><?php echo $data['Name'];?></td>
                  <td><?php echo $data['Email'];?></td>
                  <td><?php echo $data['Phone'];?></td>
                  <td><?php echo $data['Address'];?></td>
                  <td>
                    <a class="btn btn-primary"  href="<?php echo site_url('/Customer/customer_edit/'.$data['CID']); ?>"><i class="fa fa-edit"></i></a>
                   <a class="btn btn-danger"  href="<?php echo site_url('/Customer/customer_delete/'.$data['CID']); ?>"><i class="glyphicon glyphicon-trash">
                  </td>
                </tr>
              <?php endforeach;?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
