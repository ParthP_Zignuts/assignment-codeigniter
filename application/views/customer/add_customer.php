
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Customer
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Customer</a></li>
        <li class="active">Add Customer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Customer</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open_multipart('Customer/add_Customer'); ?>
              <div class="box-body">
                
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="Name" placeholder="Name" class="form-control" required>
                </div>

                <div class="form-group">
                  <label>Email</label>

                  <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-envelope"></i>
                        </div>
                         <input type="email" class="form-control" name="Email" placeholder="Email" required>
                  </div>
                </div>
                <div class="form-group">
                      <label>Phone</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="text" name="Phone" placeholder="Phone" class="form-control">
                        </div>
                        <!-- /.input group -->
                </div>
                <div class="form-group">
                  <label>Address</label>
                  <input type="text" class="form-control" name="Address" placeholder="Address" required>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>


         