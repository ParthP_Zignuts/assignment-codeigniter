
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Change Passowerd
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
        <li class="active">Change Passowerd</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
       
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo validation_errors(); ?>

           <?php echo form_open_multipart('Admin/update_pwd/'.$admin[0]['id']); ?>

              <div class="box-body">

                <div class="form-group">
                  <label>Enter Old Passowerd</label>
                  <input type="password" name="old_pwd" class="form-control" required>
                </div>

                <div class="form-group">
                  <label>Enter New Password</label>
                  <input type="password" class="form-control" name="new_pwd" required>
                </div>

                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control" name="cnf_pwd" required>
                </div>

             </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>


         