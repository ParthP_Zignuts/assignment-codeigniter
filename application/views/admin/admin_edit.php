
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Profile Edit
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       
        <li class="active">Admin Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Admin Profile Edit</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open_multipart('Admin/admin_update/'.$admin[0]['id']); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" value="<?php echo $admin[0]['name'];?>" class="form-control" required>
                </div>

                <div class="form-group">
                  <label>Email address</label>
                  <input type="email" class="form-control" name="email" value="<?php echo $admin[0]['Email'];?>" required>
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="text" class="form-control"  value="<?php echo $admin[0]['Password'];?>" disabled>
                </div>
                
                <div class="form-group">
                  <label>Active/not</label>
                  <select class="form-control" name="Active">
                    <option value="1" <?php if($admin[0]['active']==1){ ?> selected <?php }?> >Active</option>
                    <option value="0" <?php if($admin[0]['active']==0){ ?> selected <?php }?>>Not Active</option>
                  </select> 
                </div>
              

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>


         