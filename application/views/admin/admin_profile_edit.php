
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin profile edit
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
       
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open_multipart('Admin/update_profile/'.$admin[0]['id']); ?>

              <div class="box-body">

                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" value="<?php echo $admin[0]['name'];?>" class="form-control">
                </div>

                <div class="form-group">
                  <label>Email address</label>
                  <input type="email" class="form-control" name="email" value="<?php echo $admin[0]['Email'];?>">
                </div>

                <div class="form-group">
                  <label>Active/not</label>
                  <input type="text" class="form-control" name="active" value="<?php if($admin[0]['active']==1){ ?> Active <?php }else { ?> Not Active <?php }?> " disabled>
                  
                </div>
              

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
          </div>
        </div>
      </section>
    </div>
  </div>
  

         