<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>  
<head>
  <title>Assigement Codeigniter</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="<?php  echo base_url(); ?>/assets/css/styles.css">

</head>
<body>
<div class="container">

  <?php if($this->session->flashdata('login_failed')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Error!</strong> Enter valid Email and Passwerd.
       </div>

  <?php endif; ?>

  <?php if($this->session->flashdata('logged_out')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> Successfully Logout.
       </div>

  <?php endif; ?>

  <?php if($this->session->flashdata('user_registered')):?>

        <div class="alert alert-success alert-dismissible">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> Successfully Registrated.
       </div>

  <?php endif; ?>

  

  <div class="d-flex justify-content-center h-100">

     <div class="card">

      <div class="card-header">
        <h3>Sign In</h3>
        
      </div>
      <div class="card-body">

       <?php echo form_open_multipart('Admin/login_valid'); ?>
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="email" class="form-control" name="email" placeholder="email" required>
            
          </div>
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" class="form-control" name="password" placeholder="password" required>
          </div>
          
          <div class="form-group">
            <input type="submit" value="Login" class="btn float-right login_btn">
          </div>
        <?php echo form_close();?>
      </div>
      <div class="card-footer">
        <div class="d-flex justify-content-center links">
          Don't have an account?<a href="Admin/sign_up">Sign Up</a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
192.168.0.119/Assignment_Codeigniter