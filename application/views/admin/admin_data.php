
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Data Tables
        <small>advanced tables</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Admin Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Active / not</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>

            <?php foreach($admin as $data):?>
                <tr>
                  <td><?php echo $data['id'];?></td>
                  <td><?php echo $data['name'];?></td>
                  <td><?php echo $data['Email'];?></td>
                  <td><?php echo $data['Password'];?></td>
                  <td><?php if($data['active']==1)echo 'Active';else echo 'Not Active';?></td>
                  <td>
                    <a class="btn btn-primary"  href="<?php echo site_url('/Admin/admin_edit/'.$data['id']); ?>"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger" href="<?php echo base_url('/Admin/admin_delete/'.$data['id']); ?>" ><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
              <?php endforeach;?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Active / not</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

