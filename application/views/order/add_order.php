<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Order
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Orders</a></li>
        <li class="active">Add Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Order</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open_multipart(); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Order Number</label>
                  <input type="text" name="OrderNumber" id="OrderNumber" placeholder="Order Number" class="form-control" value="<?php echo $this->session->tempdata('OrderNumber') ?>" required>
                </div>

               <div class="form-group">
                  <label>Customer</label>
                  <select class="form-control" name="Customer" id="Customer">
                   <?php foreach($Customer as $customer): ?>

                    <option value="<?php echo $customer['CID']; ?>"><?php echo $customer['Name']; ?></option>

                   <?php endforeach; ?>
                  </select>
                </div>
                 
            </div>
              <!-- /.box-body -->

             <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-7">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Cart</h3><span class="glyphicon glyphicon-shopping-cart"></span>

                <button type="submit" id="add_item" class="btn btn-primary pull-right">Add</button>
            
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('Order/place_order'); ?>
              <div class="box-body">
                <table id="itemTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Item</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Subtotal</th>
                      <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                      

                          <tr id="row_1">
                              <td>
                                <select onchange="add_cart(this)" class="form-control item"  name="Item[]">
                                 <?php foreach($Items as $item): ?>

                                  <option value="<?php echo $item['ID'];?>"><?php echo $item['Name']; ?></option>

                                 <?php endforeach; ?>
                                </select>
                              </td>
                              <td><input  type="text" class="form-control Price" name="Price[]" readonly></td>
                              <td><input  min="1" onchange="change_qty(this)" type="number" class="form-control Qty" name="Qty[]" placeholder="Quantity" required></td>
                              <td><input  type="text" class="form-control sub_total" name="sub_total[]" readonly></td>
                              <td>
                                    <a class="btn btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                          </tr>

                        
                    </tbody>
                    <tfoot>
                      <tr class="total">
                            <th colspan="3" style="">Total</th>
                            <th colspan="2"><input type="number" class="form-control Total_Price" name="total_price" readonly></th>
                          </tr>
                    </tfoot>
                </table>
               
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <a href="<?php echo base_url('Order/Cancel_Order');?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary pull-right">Order Place</button>
              </div>
              <!-- /.box-footer -->
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        
        </div>

      </div>
    </section>
  </div>
</div>
<script type="text/javascript">

  $(document).ready(function(){
    var count=1;
     $('#add_item').click(function() {
        count=count+1;
         $('#itemTable tbody').append('<tr id="row_'+count+'"><td><select onchange="add_cart(this)" class="form-control item"  name="Item[]"><?php foreach($Items as $item): ?><option value="<?php echo $item['ID'];?>"><?php echo $item['Name']; ?></option><?php endforeach; ?></select></td><td><input  type="text" class="form-control Price" name="Price[]" readonly></td><td><input  min="1" onchange="change_qty(this)" type="number" class="form-control Qty" name="Qty[]" placeholder="Quantity" required></td><td><input  type="text" class="form-control sub_total" name="sub_total[]" readonly></td><td><a class="btn btn-danger" onclick="del_item(this)" ><i class="glyphicon glyphicon-trash"></i></a></td></tr>');
      });

  })
   

function add_cart(item){
  var item_id = item.value;
  var tr = $(item).parent().parent();
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url(); ?>Order/add_order_ajx/"+item_id, 
                
                dataType: 'json',
                    success: function (data) {
                      tr.find(".Price").val((data[0].Price));
                      tr.find(".Qty").val(1);
                      tr.find(".sub_total").val(tr.find(".Price").val()*tr.find(".Qty").val());
                      total_of_total();
                      //alert(data[0].Price);
                    },

            });
}

var total_of_total= function (){

       var sum=0;

       $(".sub_total").each(function (){

         var t= $(this).val().replace(',','');
            //alert(t);
         if(t != 0){
             sum += parseFloat(t);
         }

       $(".Total_Price").val(sum);
       });

}

$(document).on("change","#OrderNumber,#Customer",function(){
 
    var OrderID = $('#OrderNumber').val();
    var CustomerId = $('#Customer').val();
    
      $.ajax({
                url: "<?php echo base_url(); ?>Order/add_cart/",
                type: 'POST',
                data: {"OrderID":OrderID,"CustomerId":CustomerId},
                dataType: 'json',
                    

            });

 //  $.get("http://localhost/Assignment_Codeigniter/Order/add_cart/",{oid:OrderID,cid:CustomerId} ,function(){
 //   alert("Data: " + data + "\nStatus: " + status);
 // })

})

function change_qty(qty){
      var qty_no = qty.value;
      
      var tr = $(qty).parent().parent();
      
      tr.find(".sub_total").val(tr.find(".Price").val()*qty_no);
      total_of_total();
}

function del_item(item){
   var id = $(item).closest('tr').attr('id');
   $("#"+id).remove();
    total_of_total();
}
</script>

         