<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Oders Data Tables
        <small>advanced tables</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
      </ol>
  

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
         
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Orders Data Table With Full Features</h3>
              <a href="<?php echo base_url(); ?>Order\add" class="btn btn-block btn-success" style="float: right;width: auto;">Add Order</a>

            </div>
            <?php //echo "<pre>";print_r($status); ?>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Order Number</th>
                  <th>Customer</th>
                  <th>Price</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>

            <?php /*echo "<pre>";
        print_r($status);*/$c=0; foreach($orders as $order):?>
                <tr>
                  <td><?php echo $order['ID'];?></td>
                  <td><?php echo $order['OrderNumber'];?></td>
                  <td><?php echo $order['Name'];?></td>
                  <td><?php echo $order['Price'];?></td>
                  <td><?php echo $order['Address'];?></td>
                 
                  <td><label style="color:<?php echo $status[$c][0]['Color'];?>"><?php echo $status[$c][0]['Name']; ?></label></td>
                  
                      
                <td>
                    <a class="btn btn-primary" href="<?php echo base_url('/Order/View/'.$order['ID']); ?>">View</a>
                    <a class="btn btn-primary"  href="<?php echo base_url('/Order/order_edit_page/'.$order['ID']); ?>"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger"  href="<?php echo base_url('/Order/order_delete/'.$order['ID']); ?>"><i class="glyphicon glyphicon-trash">
                  </td>
                </tr>
              <?php $c++; endforeach;?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Order Number</th>
                  <th>Customer</th>
                  <th>Price</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

