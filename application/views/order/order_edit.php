

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Order
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Orders</a></li>
        <li class="active">Edit Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Order</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php 
           
          
           /*print_r($order_data['orders']); 
           echo "<br>";

          print_r($order_data['order_details']);*/

       
          $this->session->set_tempdata('OrderID',$order_data['orders'][0]['ID'],600);

          echo form_open_multipart('Order/update_order/'.$order_data['orders'][0]['OrderNumber']); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Order Number</label>
                  <input type="text" name="OrderNumber" placeholder="Order Number" class="form-control" value="<?php echo $order_data['orders'][0]['OrderNumber'];$this->session->set_tempdata('OrderNumber',$order_data['orders'][0]['OrderNumber'],600); ?>" disabled>
                </div>

               <div class="form-group">
                  <label>Customer</label>
                  <input type="text" name="OrderNumber" placeholder="Order Number" class="form-control" value="<?php echo $order_data['customer_name'][0]['Name'];
                  $this->session->set_tempdata('CustomerId',$order_data['customer_name'][0]['CID'],600); ?>" disabled>
                </div>
               
               <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" name="Status">
                    <option value="0"><?php echo $CurrentStatus[0]['Name'];?></option>
                   <?php foreach($Status as $status): ?>

                    <option value="<?php echo $status['ID'];?>"><?php echo $status['Name']; ?></option>

                   <?php endforeach; ?>
                  </select>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-7">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Cart</h3><span class="glyphicon glyphicon-shopping-cart"></span>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('Order/update_place_order'); ?>
              <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Item</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Subtotal</th>
                      <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                      
                          <?php $c=0; $total_price=0; foreach($order_data['temp_order_datails'] as $data): ?>
                          <tr>
                          <td>
                            <div class="form-group">
                             
                              <select class="form-control" name="Item_name[]">
                                <option value="<?php echo $item_data[$c][0]['Name']?>"><?php echo $item_data[$c][0]['Name']; ?></option>
                                 <?php foreach($Items as $item): ?>

                                  <option value="<?php echo $item['ID'];?>"><?php echo $item['Name']; ?></option>

                                 <?php endforeach; ?>
                              </select>
                              </div>
                          </td>
                              
                              <td style="width:10px;"><input type="number" id="Qty" name="Qty[]" class="form-control"value="<?php echo $data['Qty']?>"></td>

                              <td><?php echo $item_data[$c][0]['Price']?></td>
                              <td><?php echo $item_data[$c][0]['Price']*$data['Qty']?></td>
                              <td>
                                    <a class="btn btn-danger" href="<?php echo base_url('/Order/del_item_cart/'.$data['Item_Id']); ?>"><i class="glyphicon glyphicon-trash"></i></a>
                              </td>

                           </tr>

                          <?php  $c++;$total_price+=$data['Price'];
                          endforeach; ?>
                          
                          <tr>
                            <th colspan="3" style="">Total</th>
                            <th colspan="2"><?php echo $total_price; ?></th>
                          </tr>

                    </tbody>
                </table>
               
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Edit Order</button>
              </div>
              <!-- /.box-footer -->
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        
        </div>

      </div>
    </section>
  </div>
</div>


         