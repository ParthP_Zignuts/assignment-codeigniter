

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Order
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Orders</a></li>
        <li class="active">Add Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Order</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open_multipart('Order/add_cart'); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Order Number</label>
                  <input type="text" name="OrderNumber" placeholder="Order Number" class="form-control" value="<?php echo $this->session->tempdata('OrderNumber') ?>" required>
                </div>

               <div class="form-group">
                  <label>Customer</label>
                  <select class="form-control" name="Customer">
                   <?php foreach($Customer as $customer): ?>

                    <option value="<?php echo $customer['CID']; ?>"><?php echo $customer['Name']; ?></option>

                   <?php endforeach; ?>
                  </select>
                </div>
                 <div class="form-group"><?php $this->input->post('Customer'); ?>
                  <label>Items</label>
                  <select class="form-control" name="Item">
                   <?php foreach($Items as $item): ?>

                    <option value="<?php echo $item['ID'];?>"><?php echo $item['Name']; ?></option>

                   <?php endforeach; ?>
                  </select>
                </div>
                 <div class="form-group">
                  <label>Quantity</label>
                  <input type="number" class="form-control" name="Qty" placeholder="Quantity" required>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Cart</h3><span class="glyphicon glyphicon-shopping-cart"></span>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('Order/place_order'); ?>
              <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Item</th>
                      <th>Quantity</th>
                      <th>Price</th>
                      <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                      
                          <?php $total_price=0; foreach($temp_data as $data): ?>
                          <tr>
                              <td><?php echo $data['Item_Id']?></td>
                              <td><?php echo $data['Qty']?></td>
                              <td><?php echo $data['Price']?></td>
                              <td>
                                    <a class="btn btn-primary" href=""><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" href="<?php echo base_url('/Order/del_item_cart/'.$data['Item_Id']); ?>" ><i class="glyphicon glyphicon-trash"></i></a>
                              </td>
                          </tr>

                          <?php  $total_price+=$data['Price'];
                          endforeach; ?>
                          
                          <tr>
                            <th colspan="3">Total</th>
                            <th><?php echo $total_price; ?></th>
                          </tr>

                    </tbody>
                </table>
               
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary pull-right">Order Place</button>
              </div>
              <!-- /.box-footer -->
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        
        </div>

      </div>
    </section>
  </div>
</div>


         