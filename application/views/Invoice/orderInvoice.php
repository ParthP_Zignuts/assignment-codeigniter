
	
	<div class="wrapper">
<!-- Content Wrapper. Contains page content class="table table-bordered table-striped"-->
  <div class="content-wrapper">
				
  		<div class="box-body">
         <div class="container">
    <div class="row">
        <div class="col-xs-10">
    		<div class="invoice-title"><?php //echo "<pre>"; print_r($Oredr_all_data);?>
    			<h2>Invoice</h2><h3 class="pull-right"><?php echo $Oredr_all_data['order_data'][0]['OrderNumber'];?></h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
    					<?php echo $Oredr_all_data['admin_data'][0]['name']; ?><br>
    					
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<strong>Shipped To:</strong><br>
    					<?php echo $Oredr_all_data['customer_data'][0]['Name']; ?><br>
    					Email : <?php echo $Oredr_all_data['customer_data'][0]['Email']; ?><br>
    					Address : <?php echo $Oredr_all_data['customer_data'][0]['Address']; ?><br>
    					Phone : <?php echo $Oredr_all_data['customer_data'][0]['Phone']; ?>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			
    			<div class="col-xs-6 text-left">
    				<address>
    					<strong>Order Date:</strong><br>
    					<?php echo $Oredr_all_data['order_data'][0]['Date']; ?><br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-10">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<?php $c=0;foreach ($Oredr_all_data['order_details'] as $data): ?>
    							<tr>
    								
    								<td>
    									<?php echo $Oredr_all_data['item_data'][$c][0]['Name'];?></td>
    								<td class="text-center"><?php echo $Oredr_all_data['item_data'][$c][0]['Price'];?></td>
    								

    								<td class="text-center"><?php echo $Oredr_all_data['order_details'][$c]['Qty'];?></td>
    								<td class="text-right"><?php echo $Oredr_all_data['order_details'][$c]['Price'];?></td>
    							</tr>
                                <?php $c++; endforeach; ?>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Total</strong></td>
    								<th class="thick-line text-right"><?php echo $Oredr_all_data['order_data'][0]['Price'];?></th>
    							</tr>
    					  </tbody>
    					</table>
					</div>
				 </div>
    		  </div>
    		<a href="<?php echo base_url('/Order/sendEmail/'.$Oredr_all_data['order_data'][0]['ID']);?>" class="btn btn-danger pull-left"><i class="fa fa-upload"></i> Send Pdf</a>

    		<a href="<?php echo base_url('/Order/pdfdetails/'.$Oredr_all_data['order_data'][0]['ID']);?>" class="btn btn-primary pull-right" style="margin-right: 5px;">
					<i class="fa fa-download"></i> Generate PDF
			</a>
    	</div>
    </div>
			  </div>
          </div>

		</div>
	</div>
</body>
</html>
