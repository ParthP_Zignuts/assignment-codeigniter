

<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Item
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Items</a></li>
        <li class="active">Add Item</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Item</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
           <?php echo form_open_multipart('Item/add_item'); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Item Number</label>
                  <input type="number" name="ItemNumber" placeholder="Enter Item Number" class="form-control" required>
                </div>

                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="Category">
                   <?php foreach($Category as $category): ?>

                    <option value="<?php echo $category['ID'];?>"><?php echo $category['Name']; ?></option>

                   <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="Name" value="" class="form-control" required>
                </div>

                 <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="3" name="Description" placeholder="Description" required></textarea>
                </div>
                 <div class="form-group">
                  <label>Image</label>
                  <input type="file" class="form-control" name="Image">
                </div>
                 <div class="form-group">
                  <label>Price</label>
                  <input type="number" class="form-control" name="Price" placeholder="Price" required>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>


         