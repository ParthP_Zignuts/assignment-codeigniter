
<div class="wrapper">

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items Data Tables
        <small>advanced tables</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Items Data tables</li>
      </ol>
  

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Items Data Table With Full Features</h3>
              <a href="<?php echo base_url(); ?>Item\add" class="btn btn-block btn-success" style="float: right;width: auto;">Add Item</a>

            </div>

            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Item Number</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Price</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>

            <?php foreach($items as $data):?>
                <tr>
                  <td><?php echo $data['ID'];?></td>
                  <td><?php echo $data['ItemNumber'];?></td>
                  <td style="width: 200px;"><?php echo word_limiter($data['Name'],2,'<span style="color: #243af2;" class="readmore" data-Desc="'.$data['Name'].'"> read more</span>');?></td>
                  <td><?php echo $data['Category'];?></td>
                  <td style="width: 200px;"><?php echo word_limiter($data['Description'],4,'<span style="color: #243af2;" class="readmore" data-Desc="'.$data['Description'].'"> read more</span>');?></td>
                  <td><img src="<?php echo base_url().'uploads/'.$data['Image']; ?>" width='60px' height='60px' class="img-responsive"></td>
                  <td><?php echo $data['Price'];?></td>
                  <td style="text-align: center;">
                    <a class="btn btn-primary"  href="<?php echo site_url('/Item/item_edit/'.$data['ID']); ?>"><i class="fa fa-edit"></i></a>
                     <a class="btn btn-danger"  href="<?php echo site_url('/Item/item_delete/'.$data['ID']); ?>"><i class="glyphicon glyphicon-trash"></i></a>
                    
                  </td>
                </tr>
              <?php endforeach;?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Item Number</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Price</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <div class="control-sidebar-bg"></div>
</div>
<!-- dialog box for read more discription -->
    <div class="modal fade" id="modelWindow" role="dialog">
            <div class="modal-dialog modal-sm vertical-align-center">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Description</h4>
                </div>
                <div class="modal-body">
                    <span class="Description_view"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                </div>
              </div>
            </div>
        </div>

