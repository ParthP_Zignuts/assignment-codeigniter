
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Item
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Items</a></li>
        <li class="active">Edit Item</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Item</h3>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
           
           <?php echo form_open_multipart('Item/update_item/'.$items[0]['ItemNumber']); ?>
              <div class="box-body">

                <div class="form-group">
                  <label>Item Number</label>
                  <input type="number" name="ItemNumber" placeholder="Enter Item Number" class="form-control" value="<?php echo $items[0]['ItemNumber']; ?>" required disabled>
                </div>

               <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" name="Category">
                    <option value="<?php echo $Category_id[0]['ID']; ?>"><?php echo $Category_id[0]['Name']; ?></option>

                   <?php foreach($Category as $data): ?>

                    <option value="<?php echo $data['ID'];?>"><?php echo $data['Name']; ?></option>

                   <?php endforeach; ?>
                  </select>
                </div>

              <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="Name" value="<?php echo $items[0]['Name'];?>" class="form-control" required>
              </div>

              <div class="form-group">
                  <label>Description</label>
                  <input type="text" class="form-control" rows="3" value="<?php echo $items[0]['Description'];?>" name="Description"  required>
              </div>

              <div class="form-group">
                  <label>Image</label>
                <div class="col-md-12">
                  <div class="col-md-3">
                    <img src="<?php echo base_url().'uploads/'.$items[0]['Image']; ?>" width='60px' height='60px' class="img-responsive">
                    <?php $this->session->set_tempdata('OldImage',$items[0]['Image'],600); ?>
                  </div>
                  <div class="col-md-9">
                    <input type="file" class="form-control" name="Image">
                  </div>
                </div>
              </div>

                <div class="form-group">
                  <label>Price</label>
                  <input type="number" value="<?php echo $items[0]['Price'];?>" class="form-control" name="Price" placeholder="Price" required>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
</div>


         